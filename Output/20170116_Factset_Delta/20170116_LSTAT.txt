EDI_LSTAT_20170116
TableName	Actflag	Created	Changed	LstatID	SecID	ISIN	ExchgCD	NotificationDate	EffectiveDate	LStatStatus	EventType	Reason
LSTAT	I	2017/01/16	2017/01/16	5337692	4872088	BRPRBCLFI115	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337650	3397463	KR35120171C6	KRKSE	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/14	2017/01/16	5337475	2563728	KR6060001130	KRKSE	2016/11/24	2016/12/26	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337720	3955047	US3135G0SJ36	USTRCE	2016/12/16	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	U	2017/01/12	2017/01/16	5336182	3746324	XS0776123514	IEISE	2016/12/21	2016/12/21	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/14	2017/01/16	5337566	3746324	XS0776123514	IEISE	2016/12/22	2016/12/22	R		No further information
LSTAT	I	2017/01/14	2017/01/16	5337567	3746324	XS0776123514	IEISE	2016/11/22	2016/12/23	D	REDEM	Final Redemption of the security.
LSTAT	I	2017/01/16	2017/01/16	5337737	4288114	XS1078730584	LULSE	2016/11/20	2016/12/21	D	REDEM	Final Redemption of the security.
LSTAT	I	2017/01/16	2017/01/16	5337734	4288114	XS1078730584	LULSE	2016/12/20	2016/12/20	R		No further information
LSTAT	I	2017/01/16	2017/01/16	5337735	4288114	XS1078730584	DEBSE	2016/11/20	2016/12/21	D	REDEM	Final Redemption of the security.
LSTAT	I	2017/01/16	2017/01/16	5337736	4288114	XS1078730584	DEFSX	2016/11/20	2016/12/21	D	REDEM	Final Redemption of the security.
LSTAT	U	2017/01/10	2017/01/16	5335103	4288114	XS1078730584	LULSE	2016/12/19	2016/12/19	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337732	4288114	XS1078730584	DEBSE	2016/12/20	2016/12/20	R		No further information
LSTAT	I	2017/01/16	2017/01/16	5337733	4288114	XS1078730584	DEFSX	2016/12/20	2016/12/20	R		No further information
LSTAT	U	2017/01/10	2017/01/16	5335101	4288114	XS1078730584	DEBSE	2016/12/19	2016/12/19	D	REDEM	Final Redemption of the Security.
LSTAT	U	2017/01/10	2017/01/16	5335102	4288114	XS1078730584	DEFSX	2016/12/19	2016/12/19	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/14	2017/01/16	5337594	4925025	XS1498422259	GBLSE	2017/01/13	2016/12/23	D	REDEM	Final Redemption of the security.
LSTAT	I	2017/01/16	2017/01/16	5337679	4791460	BRBBDCLFN7G3	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337666	4614680	BRNVTNDBS009	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337625	4424384	CND100008093	CNIBBM	2016/11/25	2016/12/26	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337664	4562541	INE722A07646	INBSE	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337820	4441581	INE918K07797	INBSE	2017/01/18	2017/01/18	S		No further information
LSTAT	I	2017/01/16	2017/01/16	5337635	4713340	KR310505A623	KRKSE	2016/11/25	2016/12/26	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/14	2017/01/16	5337604	3444844	US78573AAA88	IEISE	2017/01/11	2017/01/11	D	CORR	No further information
LSTAT	I	2017/01/14	2017/01/16	5337605	3444859	US78573AAC45	IEISE	2017/01/11	2017/01/11	D	CORR	No further information
LSTAT	I	2017/01/16	2017/01/16	5337925	4288040	US87238QAA13	USOTC	2017/01/16	2015/06/16	D	CONV	Final Conversion of the security.
LSTAT	I	2017/01/16	2017/01/16	5337926	4288040	US87238QAA13	USTRCE	2017/01/16	2015/06/16	D	CONV	Final Conversion of the security.
LSTAT	I	2017/01/14	2017/01/16	5337603	4088784	USU7787RAG66	IEISE	2017/01/11	2017/01/11	D	CORR	No further information
LSTAT	I	2017/01/16	2017/01/16	5337691	4872034	BRPINELFI2P5	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337716	4945782	BRZXSZLFN786	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337652	3427246	CND100004TH4	CNIBBM	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337653	3427246	CND100004TH4	CNSGSE	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337842	4197351	INE872A07SJ4	INNSE	2017/01/16	2017/01/19	D	REDEM	Redemption (Final)
LSTAT	I	2017/01/16	2017/01/16	5337824	4447746	INE918K07821	INBSE	2017/01/25	2017/01/25	S		No further information
LSTAT	U	2017/01/12	2017/01/16	5336659	3455909	US44106M6075	USNASD	2017/02/10	2017/02/10	S	REDEM	(As on 12/01/2017) STKHS<BR>Hospitality Properties Trust Announces Redemption of 11.6 Million Outstanding 7.125% Series D Cumulative Redeemable Preferred Shares<BR>Hospitality Properties Trust (Nasdaq: HPT) today announced that it will redeem all of its 11.6 million outstanding 7.125% Series D Cumulative Redeemable Preferred Shares (CUSIP No.: 44106M607 and Nasdaq: HPTRP) (`Series D Preferred Shares`) at the stated Liquidation Preference price of USD25 per share plus accrued and unpaid dividends to the date of redemption. This redemption is expected to occur on or about February 10, 2017. Dividends will cease to accrue on the Series D Preferred Shares as of the redemption date. Holders who hold Series D Preferred Shares through the Depository Trust Company (DTC) will be redeemed in accordance with DTC`s procedures. Questions relating to the notice of redemption and related materials should be directed to Wells Fargo Shareowner Services, HPT`s transfer agent and the paying agent for the redemption of the Series D Preferred Shares, at 1-800-468-9716. The address of the paying agent is Wells Fargo Shareowner Services, Attn: Corporate Actions Department, P.O. Box 64858, St. Paul, MN 55164-0858.<BR>HPT expects to fund this redemption with cash on hand and borrowings under its revolving credit facility.<BR>
LSTAT	I	2017/01/16	2017/01/16	5337678	4789266	BRBCEFLFIM32	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337665	4614676	BRNVFEDBS007	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337667	4614682	BRNVMCDBS007	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337689	4852818	BRSANBLFIIB4	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/14	2017/01/16	5337480	4184831	CND1000075C9	CNIBBM	2016/11/24	2016/12/26	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/14	2017/01/16	5337598	3602257	DE000BRL8602	DEHSE	2017/02/23	2017/02/23	D		No further information
LSTAT	I	2017/01/14	2017/01/16	5337595	4429077	DE000WGZ7ZX9	DEDSE	2017/01/12	2017/01/12	D		No further information
LSTAT	I	2017/01/14	2017/01/16	5337478	4166048	IDA0000616B3	IDJSE	2016/11/25	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337825	4654216	INE918K07DO6	INBSE	2017/02/08	2017/02/08	S		No further information
LSTAT	I	2017/01/16	2017/01/16	5337673	4714645	KR310506A621	KRKSE	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/14	2017/01/16	5337502	4736547	KR380809A632	KRKSE	2016/11/24	2016/12/26	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337681	4802434	BRBPACLFIL35	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/14	2017/01/16	5337473	1721089	CND100004HG1	CNIBBM	2016/11/24	2016/12/26	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337844	4197377	INE872A07SM8	INNSE	2017/01/16	2017/01/19	D	REDEM	Redemption (Final)
LSTAT	I	2017/01/14	2017/01/16	5337522	4672700	KR310207J5C2	KRKSE	2016/12/23	2016/12/26	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/14	2017/01/16	5337484	4543633	KR6000016560	KRKSE	2016/11/24	2016/12/26	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337624	4334438	KR6019806480	KRKSE	2016/11/25	2016/12/26	D	REDEM	Final Redemption of the Security.
LSTAT	I	2016/07/22	2016/07/24	5245962	4833532	US45950VJK35	DEBSE	2016/07/22	2016/07/22	D	LSTAT	No further information
LSTAT	I	2017/01/16	2017/01/16	5337728	4377372	US532776AZ45	USNYSE	2017/01/17	2017/01/17	S		Traded Bonds - Trading to be Suspended  01/17/2017<BR>The following Listed bonds will be suspended from trading prior to the open on Tuesday, January 17, 2017 and will be removed from the NYSE list prior to the opening due to merger.
LSTAT	I	2017/01/16	2017/01/16	5337889	4030076	USG9844KAA72	HKSEHK	2017/01/16	2017/01/16	S		(As On 16/01/2017)HKSEHK<BR><BR>EXCHANGE NOTICE - TRADING HALT <BR>Trading in the shares (stock code: 02168) and debt securities (stock codes: 05793 and 05926) of Yingde Gases Group Company Limited will be halted at 9:00 a.m. today (16/1/2017).<BR>
LSTAT	U	2016/10/25	2017/01/16	5292917	233169	XS0170738263	LULSE	2016/10/04	2016/10/04	D	CONV	Final Redemption-Conversion of the Security.
LSTAT	I	2017/01/14	2017/01/16	5337572	233169	XS0170738263	LULSE	2016/10/05	2016/10/05	R		No further information
LSTAT	I	2017/01/16	2017/01/16	5337878	651217	XS0214398199	DEBSE	2017/01/16	2017/01/16	D	LSTAT	No further information
LSTAT	I	2017/01/16	2017/01/16	5337749	2178420	XS0553737445	LULSE	2016/11/21	2016/12/21	D	REDEM	Final Redemption of the security.
LSTAT	U	2017/01/10	2017/01/16	5335005	2178420	XS0553737445	LULSE	2016/12/19	2016/12/19	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337748	2178420	XS0553737445	LULSE	2016/12/20	2016/12/20	R		No further information
LSTAT	I	2017/01/16	2017/01/16	5337680	4797041	BRBBDCLFN7V2	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337688	4833635	BRBBDCLFNPL1	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337668	4615890	BRNVPEDBS006	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337647	316092	CND0000002J0	CNIBBM	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/14	2017/01/16	5337479	4166058	IDA0000614A0	IDJSE	2016/11/24	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337651	3397754	INE752E07JC4	INNSE	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/14	2017/01/16	5337529	1362800	US25468PCK03	DEBSE	2017/01/13	2017/01/13	D		No further information
LSTAT	U	2017/01/13	2017/01/16	5337303	346709	US9134563074	USOTC	2017/01/12	2017/01/17	D	CONV	(As on 13/01/2017) USFINRA <BR>Daily List Date 13/01/2017 10:32<BR>Type Other<BR>New Symbol <BR>Old Symbol UVVZP<BR>New Company Name <BR>Old Company Name Universal Corporation 6.75% Convertible Perpetual Preferred Stock, Series B<BR>Effective Date 13-Jan-17<BR>Comments REVISED ANNOUNCEMENT - please refer to earlier announcement on today`s Daily List;  for every 1 share of UVVZP, shareholders have the right to receive the product of the conversion rate, 22.4306, and the average volume weighted average price of the Companyaeut s common stock during the cash settlement averaging period<BR>Unit of Trade 0<BR>Old Unit of Trade 1<BR><BR>(As on 13/01/2017) USFINRA <BR>Daily List Date 13/01/2017 09:56<BR>Type Other<BR>New Symbol <BR>Old Symbol UVVZP<BR>New Company Name <BR>Old Company Name Universal Corporation 6.75% Convertible Perpetual Preferred Stock, Series B<BR>Effective Date 13-Jan-17<BR>Comments Mandatory conversion; the Company has elected to settle its conversion obligation in cash; holders of the Series B Preferred Stock will receive the product of the conversion rate, 22.3920, and the average volume weighted average price of the Companyaeut s common stock during the cash settlement averaging period<BR>Unit of Trade 0<BR>Old Unit of Trade 1<BR><BR>(As on 13/01/2017) USFINRA <BR>Daily List Date 13/01/2017 09:55<BR>Type Other<BR>New Symbol UVVZP<BR>Old Symbol UVVZP<BR>New Company Name Universal Corporation 6.75% Convertible Perpetual Preferred Stock, Series B<BR>Old Company Name Universal Corporation 6.75% Convertible Perpetual Preferred Stock, Series B<BR>Effective Date 13-Jan-17<BR>Comments Mandatory conversion; the Company has elected to settle its conversion obligation in cash; for every 1 share of UVVZP, shareholders have the right to receive the product of the conversion rate, 22.3920, and the average volume weighted average price of the Companyaeut s common stock during the cash settlement averaging period<BR>Unit of Trade 1<BR>Old Unit of Trade 1<BR><BR><BR>Final Redemption-Conversion of the security.
LSTAT	I	2017/01/16	2017/01/16	5337722	4912865	XS1487744556	LULSE	2016/12/23	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337677	4783410	BRBCGMLFI172	BRBVSP	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/14	2017/01/16	5337474	2337237	CND100003FZ7	CNIBBM	2016/11/24	2016/12/26	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337662	4542917	KR310219G560	KRKSE	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337654	4167788	KR60082783C2	KRKSE	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337879	4617726	USU42832AJ16	DEBSE	2017/01/16	2017/01/16	D		No further information
LSTAT	I	2017/01/14	2017/01/16	5337602	4088778	USU7787RAF83	IEISE	2017/01/11	2017/01/11	D	CORR	No further information
LSTAT	I	2017/01/14	2017/01/16	5337482	4408315	XS1156324342	LULSE	2016/11/24	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337725	4987760	XS1530732525	LULSE	2017/01/16	2017/01/17	D	ASSM	Assimilation
LSTAT	I	2017/01/16	2017/01/16	5337671	4670739	IN002015Z204	INRBI	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337669	4670739	IN002015Z204	INBSE	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337670	4670739	IN002015Z204	INNSE	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337843	4197367	INE872A07SK2	INNSE	2017/01/16	2017/01/19	D	REDEM	Redemption (Final)
LSTAT	I	2017/01/16	2017/01/16	5337660	4416016	KR60194534C4	KRKSE	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337672	4672680	KR60296915C2	KRKSE	2016/11/26	2016/12/27	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337719	4165058	TW0004153101	TWOTC	2016/11/26	2016/12/27	D	REDEM	Final Redemption-Conversion of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337888	4333098	USG9844KAB55	HKSEHK	2017/01/16	2017/01/16	S		(As On 16/01/2017)HKSEHK<BR><BR>EXCHANGE NOTICE - TRADING HALT <BR>Trading in the shares (stock code: 02168) and debt securities (stock codes: 05793 and 05926) of Yingde Gases Group Company Limited will be halted at 9:00 a.m. today (16/1/2017).<BR>
LSTAT	I	2017/01/14	2017/01/16	5337592	4280621	USU1570AAA08	USOTC	2017/01/13	2014/11/17	D	REDEM	Final Redemption of the security.
LSTAT	I	2017/01/14	2017/01/16	5337593	4280621	USU1570AAA08	USTRCE	2017/01/13	2014/11/17	D	REDEM	Final Redemption of the security.
LSTAT	I	2017/01/16	2017/01/16	5337622	1902888	XS0508462370	GBLSE	2016/11/25	2016/12/28	D	REDEM	Final Redemption of the Security.
LSTAT	I	2017/01/16	2017/01/16	5337723	4282449	XS1037645139	LULSE	2016/12/27	2016/12/27	D	REDEM	Final Redemption of the Security.
EDI_ENDOFFILE
