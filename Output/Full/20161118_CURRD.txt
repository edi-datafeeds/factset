EDI_CURRD_20161118
TableName	Actflag	Created	Changed	CurrdID	SecID	ISIN	EffectiveDate	OldCurenCD	NewCurenCD	OldParValue	NewParValue	EventType	Notes
CURRD	I	2001/12/26	2001/12/26	553	53726	IT0004810062	2001/12/14	ITL	EUR	1000.00000	1.00000		
CURRD	I	2003/04/11	2003/04/11	2647	64844	DE0005785836		NAD	EUR				
CURRD	I	2006/12/28	2006/12/28	5934	446481	XS0277799648	2007/01/01	ROL	RON	35000.00000	3.50000	CURRD	
CURRD	I	2006/12/28	2006/12/28	5935	454788	XS0271655325	2007/01/01	ROL	RON	1000.00000	0.10000	CURRD	
CURRD	I	2007/07/03	2007/07/03	11157	65363	RU0009029557	2007/07/03	RUR	RUB	60.00000	60.00000	CURRD	Redenomination of Currency from 1 RUR to 1 RUB
CURRD	I	2007/12/29	2007/12/29	13128	191306	MT0000071218	2008/01/01	MTL	EUR			CURRD	Currency Redenomination from MTL to EUR<BR>(0.4293 MTL = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13165	191417	MT0000011164	2008/01/01	MTL	EUR	1000.00000	2329.37340	CURRD	Currency Redenomination from MTL to EUR<BR>(0.4293 MTL = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13170	399380	MT0000011321	2008/01/01	MTL	EUR	1000.00000	2329.37340	CURRD	Currency Redenomination from MTL to EUR<BR>(0.4293 MTL = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13172	609947	MT0000081225	2008/01/01	MTL	EUR			CURRD	Currency Redenomination from MTL to EUR<BR>(0.4293 MTL = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13173	609968	MT0000081217	2008/01/01	MTL	EUR			CURRD	Currency Redenomination from MTL to EUR<BR>(0.4293 MTL = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13174	610009	MT0000031212	2008/01/01	MTL	EUR			CURRD	Currency Redenomination from MTL to EUR<BR>(0.4293 MTL = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13175	611910	MT0000331208	2008/01/01	MTL	EUR			CURRD	Currency Redenomination from MTL to EUR<BR>(0.4293 MTL = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13176	612791	MT0000011347	2008/01/01	MTL	EUR			CURRD	Currency Redenomination from MTL to EUR<BR>(0.4293 MTL = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13182	714210	MT0000361205	2008/01/01	MTL	EUR	1000.00000	2329.37340	CURRD	Currency Redenomination from MTL to EUR<BR>(0.4293 MTL = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13190	195014	CY0048450817	2008/01/01	CYP	EUR	50.00000	85.43007	CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13191	195016	CY0048580811	2008/01/01	CYP	EUR	50.00000	85.43007	CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13194	195022	CY0048440818	2008/01/01	CYP	EUR	50.00000	85.43007	CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13195	195025	CY0048610816	2008/01/01	CYP	EUR	50.00000	85.43007	CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13214	273531	CY0048880815	2008/01/01	CYP	EUR	50.00000	85.43007	CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13223	293070	CY0049510817	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13224	293370	CY0049070812	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13225	293374	CY0049060813	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13245	381954	CY0049360817	2008/01/01	CYP	EUR	1000.00000	1708.60144	CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13247	382121	CY0049390814	2008/01/01	CYP	EUR	1.00000	1.70860	CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13248	399879	CY0049930817	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13262	580581	CY0049240811	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13263	580584	CY0049420819	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13264	580585	CY0049450816	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13265	580587	CY0049560812	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13266	580589	CY0049620814	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13267	580590	CY0049710813	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13268	580591	CY0049770817	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13269	580596	CY0049790815	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13270	580599	CY0049880814	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13271	580603	CY0049990811	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13272	580607	CY0140090818	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13273	580609	CY0140160819	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13275	580617	CY0048870816	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13276	580621	CY0049250810	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13277	580624	CY0049570811	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13278	580627	CY0049630813	2008/01/01	CYP	EUR			CURRD	Currency Redenomination from CYP to EUR<BR>(0.585274 CYP = 1 EUR)
CURRD	I	2007/12/29	2007/12/29	13315	490679	XS0238679236	2008/01/01	VEB	VEF			CURRD	Currency Redenomination from VEB to VEF<BR>(1000 VEB = 1 VEF)
CURRD	I	2007/12/29	2007/12/29	13321	532024	XS0275935426	2008/01/01	VEB	VEF			CURRD	Currency Redenomination from VEB to VEF<BR>(1000 VEB = 1 VEF)
CURRD	I	2014/01/01	2014/01/01	14983	2087197	LV0000570083	2014/01/01	LVL	EUR	100.00000	0.01000	CURRD	Redenomination.
CURRD	I	2014/01/01	2014/01/01	14984	3962408	LV0000560050	2014/01/01	LVL	EUR	100.00000	0.01000	CURRD	Redenomination.
CURRD	I	2014/01/01	2014/01/01	14985	4121300	LV0000570117	2014/01/01	LVL	EUR	100.00000	0.01000	CURRD	Redenomination.
CURRD	I	2014/01/01	2014/01/01	14987	2887941	LV0000570091	2014/01/01	LVL	EUR	100.00000	0.01000	CURRD	Redenomination.
CURRD	I	2014/01/01	2014/01/01	14988	3498557	LV0000570109	2014/01/01	LVL	EUR	100.00000	0.01000	CURRD	Redenomination.
CURRD	I	2014/01/01	2014/01/01	14990	3762509	LV0000580058	2014/01/01	LVL	EUR	100.00000	0.01000	CURRD	Redenomination.
CURRD	I	2014/01/01	2014/01/01	14992	2431636	LV0000580041	2014/01/01	LVL	EUR	100.00000	0.01000	CURRD	Redenomination.
CURRD	I	2014/01/01	2014/01/01	15005	2229527	LV0000800878	2014/01/01	LVL	EUR	1000.00000	0.01000	CURRD	Redenomination.
CURRD	I	2015/01/02	2015/01/02	15103	1922107	LT0000405052	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.
CURRD	I	2015/01/02	2015/01/02	15105	1893514	LT0000605107	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.
CURRD	I	2015/01/02	2015/01/02	15106	2600684	LT0000607053	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.<BR><BR><BR><BR>
CURRD	I	2015/01/02	2015/01/02	15107	3212031	LT0000605115	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.<BR><BR><BR><BR>
CURRD	I	2015/01/02	2015/01/02	15108	3605806	LT0000603243	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.<BR><BR><BR><BR>
CURRD	I	2015/01/02	2015/01/02	15109	3827533	LT0000610057	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.<BR><BR><BR><BR>
CURRD	I	2015/01/02	2015/01/02	15110	3855100	LT0000605123	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.<BR><BR><BR><BR>
CURRD	I	2015/01/02	2015/01/02	15111	3928984	LT0000607061	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.<BR><BR><BR><BR>
CURRD	I	2015/01/02	2015/01/02	15117	3975876	LT0000603250	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.<BR><BR><BR><BR>
CURRD	I	2015/01/02	2015/01/02	15143	4109251	LT0000603268	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.
CURRD	I	2015/01/02	2015/01/02	15144	4109254	LT0000605131	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.
CURRD	I	2015/01/02	2015/01/02	15146	4125637	LT0000607087	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.
CURRD	I	2015/01/02	2015/01/02	15150	4138287	LT0000605149	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.
CURRD	I	2015/01/02	2015/01/02	15169	4225388	LT0000603276	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.
CURRD	I	2015/01/02	2015/01/02	15181	4260502	LT0000602179	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.<BR><BR><BR><BR>
CURRD	I	2015/01/02	2015/01/02	15213	4360853	LT0000600173	2015/01/01	LTL	EUR	100.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.
CURRD	I	2015/01/02	2015/01/02	15228	1754859	LT1000450015	2015/01/01	LTL	EUR	57345.00000	0.01000	CURRD	On 1st January 2015 Lithuania will adopt the Euro.<BR>The conversion rate from Lithuania Litas (LTL) to Euro (EUR) is fixed at LTL 3.4528 to 1 EUR (Conversion Rate - set by Bank of Lithuania.).<BR>Corporate Bond Nominal Value: Converts to 0,01 EUR Nominal Value.<BR>
EDI_ENDOFFILE
