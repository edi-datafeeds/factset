EDI_MPAY_20161118
TableName	Actflag	AnnounceDate	Acttime	EventType	EventID	OptionID	SerialID	ResSectyCD	ResSecID	ResISIN	ResIssuername	ResSecurityDesc	RatioNew	RatioOld	Fractions	MinOfrQty	MaxOfrQty	MinQlyQty	MaxQlyQty	Paydate	CurenCD	MinPrice	MaxPrice	TndrStrkPrice	TndrStrkStep	Paytype	DutchAuction	DefaultOpt	OptElectionDate	OedExpTime	OedExpTimeZone	WrtExpTime	WrtExpTimeZone
MPAY	I	2001/10/12	2001/10/12	LIQ	49	1	1																				F	F					
MPAY	I	2001/10/12	2001/10/12	LIQ	50	1	1																				F	F					
MPAY	I	2002/04/29	2002/04/29	LIQ	240	1	1																				F	F					
MPAY	I	2002/04/29	2002/04/29	LIQ	241	1	1													2002/04/26							F	F					
MPAY	I	2002/03/21	2002/03/21	LIQ	170	1	1																				F	F					
MPAY	I	2016/06/03	2016/06/03	LIQ	1598	1	1													2016/06/16	USD					C	F	F					
MPAY	U	2006/11/27	2007/01/09	LIQ	753	1	1	EQS												2007/03/09	ZAR	25.99000	25.99000			B	F	T					
MPAY	I	2006/12/11	2006/12/11	LIQ	753	2	1	DEB					1.0000000	10.0000000						2007/03/09	ZAR	205.85000	205.85000			B	F	F					
MPAY	I	2007/01/09	2007/01/09	LIQ	753	2	2	EQS												2007/03/09						S	F	F					
MPAY	U	2007/03/24	2007/04/10	LIQ	784	1	1													2007/05/10	USD	1.65330	1.65330			C	F	F					
MPAY	I	2008/01/09	2008/01/09	LIQ	834	1	1													2008/01/28	USD	1.60000	1.60000			C	F	F					
MPAY	U	2008/12/23	2008/12/24	LIQ	834	1	2													2009/01/15	USD	0.77000	0.77000			C	F	F					
MPAY	U	2010/09/16	2010/10/01	LIQ	834	1	3													2010/10/08	USD	0.11000	0.11000			C	F	F					
MPAY	I	2009/01/24	2009/01/24	LIQ	936	1	1														USD	5.93000	5.93000			C	F	F					
MPAY	I	2011/09/24	2011/09/24	LIQ	1229	1	1													2011/10/13	USD	6.85000	6.85000			C	F	F					
MPAY	U	2012/07/09	2012/08/29	LIQ	1229	2	1													2012/09/21	USD	6.33000	6.33000			C	F	F					
MPAY	U	2012/05/11	2012/05/14	LIQ	1265	1	1													2012/06/08	CAD	8.46794	8.46794			C	F	F					
MPAY	U	2015/05/09	2015/05/12	LIQ	1265	1	2													2015/06/02	CAD	3.54113	3.54113			C	F	F					
MPAY	U	2016/06/02	2016/06/03	LIQ	1265	1	3													2016/06/22	CAD	0.36951	0.36951			C	F	F					
MPAY	I	2015/12/12	2015/12/14	LIQ	1551	1	1													2015/12/03	USD	1.88000	1.88000			C	F	F					
MPAY	I	2016/01/16	2016/01/18	LIQ	1551	1	2	EQS					1.0000000	1.0000000						2016/01/19						S	F	F					
MPAY	I	2016/08/03	2016/08/03	LIQ	1621	1	1	WAR					0.0500400	1.0000000						2016/08/02						S	F	F					
MPAY	I	2016/08/03	2016/08/03	LIQ	1621	1	2	WAR					0.0556000	1.0000000						2016/08/02						S	F	F					
MPAY	C	2016/01/04	2016/01/05	LIQ	1560	1	1														USD					C	F	F					
MPAY	D	2007/04/25	2007/04/30	LIQ	792	1	1														USD	25.12179	25.12179			C	F	F					
MPAY	U	2007/10/11	2007/11/15	LIQ	815	1	1													2007/11/12	GBP	1.82847	1.82847			C	F	F					
MPAY	I	2007/11/15	2007/11/15	LIQ	815	2	1	ETF					3.1916219	1.0000000												S	F	F					
MPAY	I	2007/12/28	2007/12/28	LIQ	831	1	1	WAR					0.7552800	1.0000000												S	F	F					
MPAY	I	2010/03/01	2010/03/01	LIQ	1103	1	1	EQS					200000.0000000	1.0000000						2010/02/26						S	F	F					
MPAY	I	2010/03/01	2010/03/01	LIQ	1103	1	2	WAR												2010/02/26						S	F	F					
MPAY	U	2012/09/28	2012/10/03	LIQ	1306	1	1	EQS	3907514	US26817R1086	Dynegy Inc.	Ordinary Shares	0.0081393	1.0000000												S	F	F					
MPAY	U	2012/09/28	2012/10/03	LIQ	1306	1	2	WAR	3907528	US26817R1169	Dynegy Inc.	Warrants (02/10/2017)	0.1270304	1.0000000												S	F	F					
MPAY	I	2013/01/29	2013/01/29	LIQ	1333	1	1													2013/02/22	USD	5.00000	5.00000			C	F	F					
MPAY	I	2013/10/07	2013/10/07	LIQ	1333	1	2													2013/10/30	USD	1.50000	1.50000			C	F	F					
MPAY	I	2014/10/24	2014/10/24	LIQ	1333	1	3													2014/11/14	CAD	1.25000	1.25000			C	F	F					
MPAY	I	2016/10/17	2016/10/17	LIQ	1333	1	4													2016/10/28	USD	1.96000	1.96000			C	F	F					
MPAY	I	2013/03/20	2013/03/20	LIQ	1346	1	1													2013/05/15	BGN	2.55000	2.55000			C	F	F					
MPAY	I	2014/08/20	2014/08/20	LIQ	1451	1	1	EQS	3677231	US92936P1003	WMIH Corp	Ordinary Shares														S	F	F					
MPAY	I	2014/09/20	2014/09/20	LIQ	1461	1	1														USD	0.05000	0.06000			C	F	F					
MPAY	I	2014/11/08	2014/11/08	LIQ	1470	1	1														USD	18.35000	18.35000			C	F	F					
MPAY	I	2015/05/20	2015/05/20	LIQ	1470	1	2													2015/01/15	USD	2.25000	2.25000			C	F	F					
MPAY	I	2015/05/20	2015/05/20	LIQ	1470	1	3													2015/06/16	USD	1.25000	1.25000			C	F	F					
MPAY	I	2015/11/06	2015/11/06	LIQ	1470	1	4													2015/12/03	USD	1.00000	1.00000			C	F	F					
MPAY	I	2016/04/28	2016/04/28	LIQ	1470	1	5													2016/05/17	USD	2.00000	2.00000			C	F	F					
MPAY	I	2016/06/14	2016/06/14	LIQ	1470	1	6													2016/07/01	USD	1.25000	1.25000			C	F	F					
MPAY	U	2016/06/16	2016/08/03	LIQ	1470	1	7	UNT					1.0000000	1.0000000						2016/08/08						S	F	F					
MPAY	I	2016/08/17	2016/08/17	LIQ	1470	1	8													2016/08/23	USD	1.00000	1.00000			C	F	F					
MPAY	I	2016/10/18	2016/10/18	LIQ	1639	1	1														CAD					C	F	F					
MPAY	I	2015/10/26	2015/10/26	LIQ	1538	1	1	WAR					0.0574298	1.0000000												S	F	F					
MPAY	I	2016/01/16	2016/01/18	LIQ	1538	1	2	WAR					0.0434284	1.0000000						2015/12/16						S	F	F					
MPAY	D	2016/09/05	2016/09/07	LIQ	1633	1	1														EUR	171.96000	171.96000			C	F	F					
EDI_ENDOFFILE
