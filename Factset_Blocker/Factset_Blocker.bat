REM Archive current Factset BlockList

REM O:\AUTO\Scripts\vbs\FixedIncome\Prod.vbs a HAT_MS_sqlsvr1 O:\Prodman\Dev\WFI\Scripts\sql\Factset\PortfolioCoverage\BlockList.sql O:\Prodman\Dev\WFI\Feeds\Factset\Factset_Blocker\Output\PreLDateSuf.txt :_Factset_Block_List Factset_Block_List


REM Load Portfolio 991

O:\AUTO\Scripts\vbs\FixedIncome\FIpfload.vbs 991 isin HAT_MS_Sqlsvr1


REM Load Codes to BlockList from 991 Portfolio

REM O:\auto\scripts\vbs\isql.vbs HAT_MS_Sqlsvr1 O:\Prodman\Dev\WFI\Scripts\sql\Factset\PortfolioCoverage\LoadCodes.sql o:\auto\configs\dbservers.cfg Factset_Block_List Factset_Block_List


REM Update Factset Portfolio as Deleted Rows

O:\auto\scripts\vbs\isql.vbs HAT_MS_Sqlsvr1 O:\Prodman\Dev\WFI\Scripts\sql\Factset\PortfolioCoverage\Update_Factset_Portfolio.sql o:\auto\configs\dbservers.cfg Factset_Block_List Factset_Block_List
